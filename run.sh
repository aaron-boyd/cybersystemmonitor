#!/bin/bash

# --- influxdb ---
docker run --rm -dit \
    --network mynet \
    --hostname influxdb \
    --name=influxdb \
    -e INFLUXDB_DB=glances \
    -e INFLUXDB_ADMIN_USER=root \
    -e INFLUXDB_ADMIN_PASSWORD=root \
    -e INFLUXDB_USER=user \
    -e INFLUXDB_USER_PASSWORD=userpass \
    -v $PWD/influxdb:/var/lib/influxdb \
    influxdb

# --- glances ---
docker run --rm -dit \
    --network mynet \
    --hostname glances \
    --name=glances \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -v $PWD/glances:/etc/glances/ \
    --pid host \
    --env GLANCES_OPT='--config /etc/glances/glances.conf --export influxdb' \
    docker.io/nicolargo/glances

# --- grafana ---
docker run --rm -dit \
    --network mynet \
    --hostname grafana \
    --name=grafana \
    -p 3000:3000 \
    grafana/grafana
